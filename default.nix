# This file describes your repository contents.
# It should return a set of nix derivations
# and optionally the special attributes `lib`, `modules` and `overlays`.
# It should NOT import <nixpkgs>. Instead, you should take pkgs as an argument.
# Having pkgs default to <nixpkgs> is fine though, and it lets you use short
# commands such as:
#     nix-build -A mypackage

{ pkgs ? import <nixpkgs> { } }:

rec {
  # The `lib`, `modules`, and `overlays` names are special
  lib = import ./lib { inherit pkgs; }; # functions
  modules = import ./modules; # NixOS modules
  overlays = import ./overlays; # nixpkgs overlays

  # coc.nvim
  coc-jedi = pkgs.callPackage ./pkgs/coc-jedi { };
  coc-julia = pkgs.callPackage ./pkgs/coc-julia { };

  rgain3 = pkgs.python3Packages.callPackage ./pkgs/rgain3 { };

  salt-lint = pkgs.python3Packages.callPackage ./pkgs/salt-lint { };

  # vcloud
  vcd-api-schemas-type = pkgs.python3Packages.callPackage ./pkgs/vcd-api-schemas-type { };
  pyvcloud = pkgs.python3Packages.callPackage ./pkgs/pyvcloud { inherit vcd-api-schemas-type; };
  vcd-cli = pkgs.python3Packages.callPackage ./pkgs/vcd-cli { inherit pyvcloud; };

  zsh-git-prompt = pkgs.callPackage ./pkgs/zsh-git-prompt { };

  # kong
  deck = pkgs.callPackage ./pkgs/deck { };
  kong-pongo = pkgs.callPackage ./pkgs/kong-pongo { };

  # sway
  layaway = pkgs.callPackage ./pkgs/layaway { };

  # lix
  lix-updater = pkgs.callPackage ./pkgs/lix-updater { };
}
