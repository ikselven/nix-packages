{ lib
, buildGoModule
, fetchFromGitHub
, installShellFiles
, makeWrapper
}:

buildGoModule rec {
  pname = "deck";
  version = "1.41.2";

  src = fetchFromGitHub {
    owner = "Kong";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-W4shdlo+xqUBKJhbjmYIzgNGfDjMnMl2OU1yhkoHEV4=";
  };
  vendorHash =
    "sha256-o5aI37Mtia6X2L5m+xGBj/fGmjzkdfeGjPb8urXk/BU=";

  nativeBuildInputs = [
    installShellFiles
    makeWrapper
  ];

  excludedPackages = [ ];
  ldflags = [ "-s" "-w" "-X main.version=v${version}" ];

  postInstall = ''
    # opt out of analytics
    wrapProgram $out/bin/deck \
      --add-flags "--analytics=false"

    $out/bin/deck completion bash > deck.bash
    $out/bin/deck completion fish > deck.fish
    $out/bin/deck completion zsh > deck.zsh
    installShellCompletion deck.{bash,fish,zsh}
  '';

  # turn off checks because they'd spawn a k8s cluster in docker
  doCheck = false;

  meta = with lib; {
    description = "Declarative configuration for Kong";
    homepage = "https://github.com/Kong/deck";
    license = licenses.asl20;
  };
}
