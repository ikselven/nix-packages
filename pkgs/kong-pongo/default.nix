{ lib
, stdenv
, fetchFromGitHub
, makeWrapper
}:

stdenv.mkDerivation rec {
  pname = "kong-pongo";
  version = "2.14.0";

  src = fetchFromGitHub {
    owner = "Kong";
    repo = pname;
    rev = version;
    sha256 = "sha256-g33Rzff3o7kexKUetx9yGibnVkeZWnQtI7mn6il4nCk=";
  };

  nativeBuildInputs = [
    makeWrapper
  ];

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/kong-pongo
    cp -r . $out/share/kong-pongo

    mkdir -p $out/bin
    makeWrapper $out/share/kong-pongo/pongo.sh $out/bin/pongo
  '';

  dontPatchShebangs = true;

  meta = with lib; {
    description = "Tooling to run plugin tests with Kong and Kong Enterprise";
    homepage = "https://github.com/Kong/kong-pongo";
    license = licenses.asl20;
    mainProgram = "pongo";
  };
}
