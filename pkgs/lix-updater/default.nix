{ lib
, stdenvNoCC
, bash
, coreutils
, systemd
, ...
}:
stdenvNoCC.mkDerivation {
  pname = "lix-updater";
  version = "1.0";

  src = ./.;

  buildInputs = [
    bash
    coreutils
    systemd
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp lix-updater $out/bin/
    chmod 0755 $out/bin/lix-updater
  '';

  meta = with lib; {
    homepage = "https://codeberg.org/ikselven/nix-packages";
    description = "Update script for Lix for non NixOS systems";
    license = licenses.mit;
  };
}
