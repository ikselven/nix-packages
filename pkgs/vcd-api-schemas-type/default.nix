{ lib
, buildPythonPackage
, fetchPypi
}:

buildPythonPackage rec {
  pname = "vcd-api-schemas-type";
  version = "10.3.0.dev72";
  format = "wheel";

  src = fetchPypi {
    inherit version format;
    pname =
      builtins.replaceStrings [ "-" ] [ "_" ]
        pname;
    sha256 = "sha256-PyTyKmSXwDyUmru6+CZ/jzsS0FXZRoJGYt+ebl3uOC8=";
    dist = "py3";
    python = "py3";
    platform = "any";
  };

  pythonImportsCheck = [ "vcloud" ];

  meta = with lib; {
    description = "Schemas for developing against the vCloud Director APIs";
    homepage = "https://github.com/vmware/vcd-api-schemas";
    license = licenses.bsd2;
  };
}
