{ lib
, mkYarnPackage
, fetchFromGitHub
, fetchYarnDeps
}:

mkYarnPackage rec {
  name = "coc-jedi";
  packageName = "coc-jedi";
  version = "0.36.1";

  src = fetchFromGitHub {
    #url = "https://registry.yarnpkg.com/${name}/-/${name}-${version}.tgz";
    owner = "pappasam";
    repo = name;
    rev = "v${version}";
    hash = "sha256-hcws5qrhSoPooOl8zdnjpHwi84e2ga3PldU2LIL+5cs=";
  };

  offlineCache = fetchYarnDeps {
    yarnLock = src + "/yarn.lock";
    hash = "sha256-WtzbQACf6GHjxYdYpr2xsNKIeTY72D4paoULsPFaVR0=";
  };

  buildPhase = ''
    export HOME=$(mktemp -d)
    yarn --offline build
  '';

  distPhase = "true";

  installPhase = ''
    mkdir -p $out

    cp -r deps/${name}/lib $out/

    cp deps/${name}/LICENSE $out/
    cp deps/${name}/README.md $out/
    cp deps/${name}/package.json $out/
    cp deps/${name}/yarn.lock $out/
  '';

  meta = with lib; {
    description = "coc.nvim wrapper for Python's jedi-language-server";
    homepage = "https://github.com/pappasam/coc-jedi#readme";
    license = licenses.mit;
  };
}
