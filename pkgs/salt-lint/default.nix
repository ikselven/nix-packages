{ lib
, buildPythonPackage
, fetchzip
, pathspec
, pytest
, pyyaml
}:

buildPythonPackage rec {
  pname = "salt-lint";
  version = "0.9.2";

  src = fetchzip {
    url = "https://github.com/warpnet/${pname}/archive/refs/tags/v${version}.tar.gz";
    sha256 = "sha256:07hc1sjlzv1hc47qplhcychbsa4hc7nzkkf7mh7xiczal1mfbxj3";
  };

  propagatedBuildInputs = [
    pathspec
    pyyaml
  ];

  nativeCheckInputs = [
    pytest
  ];

  checkPhase = ''
    pytest tests/unit
  '';

  pythonImportsCheck = [ "saltlint" ];

  meta = with lib; {
    description = "A command-line utility that checks for best practices in SaltStack";
    homepage = "https://github.com/warpnet/salt-lint";
    license = licenses.mit;
  };
}
