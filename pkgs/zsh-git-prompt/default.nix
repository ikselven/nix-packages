# zsh-git-prompt -- Informative git prompt for zsh
#
# Usage: to enable this plugin for all users, you could
# add it to configuration.nix like this:
#
#   programs.zsh.interactiveShellInit = ''
#     source ${pkgs.zsh-git-prompt}/share/zsh-git-prompt/zshrc.sh
#   '';
#
# Or you can install it globally but only enable it in individual
# users' ~/.zshrc files:
#
#   source /run/current-system/sw/share/zsh-git-prompt/zshrc.sh
#
# Or if installed locally:
#
#   source ~/.nix-profile/share/zsh-git-prompt/zshrc.sh
#
# Either way, you then have to set a prompt that incorporates
# git_super_status, for example:
#
#   PROMPT='%B%m%~%b$(git_super_status) %# '
#
# More details are in share/doc/zsh-git-prompt/README.md, once
# installed.
#
{ fetchFromGitHub
, python3
, git
, lib
, haskellPackages
,
}:
haskellPackages.callPackage
  ({ mkDerivation
   , base
   , HUnit
   , parsec
   , process
   , QuickCheck
   ,
   }:
  mkDerivation rec {
    pname = "zsh-git-prompt";
    version = "2024-03-14";

    src = fetchFromGitHub {
      owner = "zsh-git-prompt";
      repo = "zsh-git-prompt";
      rev = "2701e8ac3192f198fcaffbc2914d8e145589eb23";
      hash = "sha256-m1RWyCnE/0tFauIDJB62jzxLBR5aKMQjW6OHY5y1a14=";
    };

    prePatch = ''
      substituteInPlace zshrc.sh                       \
        --replace-warn ':-"python"' ':-"${python3.interpreter}"' \
        --replace-warn 'git '       '${git}/bin/git '
    '';

    preCompileBuildDriver = "cd haskell";

    postInstall = ''
      cd ..
      gpshare=$out/share/${pname}
      gpdoc=$out/share/doc/${pname}
      mkdir -p $gpshare/haskell $gpdoc
      cp README.md $gpdoc
      cp zshrc.sh $gpshare
      cp -r python/ $gpshare/
      cp -r shell/ $gpshare/
      mv $out/bin $gpshare/haskell/.bin
    '';

    # unfortunately, tests are broken
    # TODO: fix tests
    doCheck = false;

    isLibrary = false;

    isExecutable = true;

    libraryHaskellDepends = [ base parsec process QuickCheck ];

    executableHaskellDepends = libraryHaskellDepends;

    testHaskellDepends = [ HUnit ] ++ libraryHaskellDepends;

    homepage = "https://github.com/zsh-git-prompt/zsh-git-prompt#readme";
    description = "Informative git prompt for zsh";
    license = lib.licenses.mit;
    maintainers = [ lib.maintainers.league ];
  })
{ }
