{ lib
, buildPythonPackage
, fetchPypi
, filetype
, mutagen
, pygobject3
, gst_all_1
, gobject-introspection
, setuptools
, pytest
, pytest-cov
, pytest-flake8
, pytest-isort
, packaging
}:

buildPythonPackage rec {
  pname = "rgain3";
  version = "1.1.1";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-ja/2t9YTo/FyDQT7MeDLJS4Y9yBnVhCQhZ4x40KZ8jo=";
  };

  build-system = [
    setuptools
    packaging
  ];

  dependencies = [
    filetype
    mutagen
    pygobject3
  ];

  optional-dependencies = [
    pytest
    pytest-cov
    pytest-flake8
    pytest-isort
  ];

  # TODO: contribute to rgain3 to be able to drop this awful patching
  postPatch = ''
    substituteInPlace setup.py \
    --replace-fail "from pkg_resources.extern.packaging.version import Version" "from packaging.version import Version"
    substituteInPlace rgain3/version.py \
    --replace-fail "from pkg_resources import DistributionNotFound, get_distribution" "from importlib.metadata import version, PackageNotFoundError" \
    --replace-fail '    __version__ = get_distribution("rgain").version' '    __version__ = version("rgain")' \
    --replace-fail "except DistributionNotFound:" "except PackageNotFoundError:"
  '';

  nativeBuildInputs = [
    gobject-introspection
  ];

  buildInputs = [
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-ugly
  ];

  pythonImportsCheck = [ "rgain3" ];

  makeWrapperArgs = [
    "--set GI_TYPELIB_PATH \"$GI_TYPELIB_PATH\""
    "--set GST_PLUGIN_SYSTEM_PATH_1_0 \"$GST_PLUGIN_SYSTEM_PATH_1_0\""
  ];

  meta = with lib; {
    homepage = "https://github.com/chaudum/rgain3";
    description = "ReplayGain tools and Python library";
    license = licenses.gpl2;
  };
}
