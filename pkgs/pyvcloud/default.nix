{ lib
, buildPythonPackage
, fetchPypi
, humanfriendly
, lxml
, packaging
, pygments
, pyyaml
, pbr
, requests
, unittest-xml-reporting
, python-dateutil
, vcd-api-schemas-type
}:

buildPythonPackage rec {
  pname = "pyvcloud";
  version = "23.0.4";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-e3M8APwORG36btNSHr/cit/tULJbE+LsPAucYdH9SVA=";
  };

  propagatedBuildInputs = [
    humanfriendly
    lxml
    packaging
    pygments
    pyyaml
    pbr
    requests
    unittest-xml-reporting
    python-dateutil
    vcd-api-schemas-type
  ];

  doCheck = false;

  # I give up....
  #checkInputs = [
  #  yapf
  #  hacking
  #  flake8-docstrings
  #  flake8-import-order
  #  pydocstyle
  #];

  #checkPhase = "";

  pythonImportsCheck = [ "pyvcloud" ];

  meta = with lib; {
    description = "Python SDK for VMware vCloud Director";
    homepage = "https://github.com/vmware/pyvcloud";
    license = licenses.asl20;
  };
}
