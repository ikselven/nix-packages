{ lib
, buildNpmPackage
, fetchFromGitHub
, esbuild
, buildGoModule
}:

let
  esbuild' = esbuild.override {
    buildGoModule = args: buildGoModule (args // rec {
      version = "0.20.2";
      src = fetchFromGitHub {
        owner = "evanw";
        repo = "esbuild";
        rev = "v${version}";
        hash = "sha256-h/Vqwax4B4nehRP9TaYbdixAZdb1hx373dNxNHvDrtY=";
      };
      vendorHash = "sha256-+BfxCyg0KkDQpHt/wycy/8CTG6YBA/VJvJFhhzUnSiQ=";
    });
  };
in
buildNpmPackage rec {
  pname = "coc-julia";
  version = "0.15.0";

  src = fetchFromGitHub {
    owner = "fannheyward";
    repo = pname;
    rev = "f1eea3f47a0114a362f6094ac9ead23291bf5507";
    hash = "sha256-kGzuh1W6x/ogF3D7iFjGWS40rRXwrPU20okuTqhxZCk=";
  };

  env = {
    ESBUILD_BINARY_PATH = lib.getExe esbuild';
  };

  npmDepsHash = "sha256-R1S0Cp1ffXDsnGeK57Dr1in4xp1ukDP6iZUEvaV/vUQ=";

  npmPackFlags = [ "--ignore-scripts" ];

  installPhase = ''
    mkdir -p $out

    cp -r lib $out/
    cp -r server $out/

    cp LICENSE $out/
    cp README.md $out/
    cp biome.json $out/
    cp esbuild.js $out/
    cp package.json $out/
  '';

  meta = with lib; {
    description = "Julia extension for coc.nvim";
    homepage = "https://github.com/fannheyward/coc-julia#readme";
    license = licenses.mit;
  };
}
