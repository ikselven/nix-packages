{ lib
, buildPythonPackage
, fetchPypi
, click
, colorama
, keyring
, pycryptodome
, pyvcloud
, tabulate
, unittest-xml-reporting
}:

buildPythonPackage rec {
  pname = "vcd-cli";
  version = "24.1.0";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-NBvlmES1igLoXZ63nVzyGNx0Ii08nP6Mzek4IV953VM=";
  };

  # if this is somehow breaking vcd-cli for you, open an issue at
  # https://github.com/vmware/vcd-cli/issues and request an update of
  # requirements.txt
  prePatch = ''
    sed --in-place --regexp-extended 's/^keyring .*$/keyring == ${keyring.version}/' requirements.txt
  '';

  propagatedBuildInputs = [
    click
    colorama
    keyring
    pycryptodome
    pyvcloud
    tabulate
    unittest-xml-reporting
  ];

  doCheck = false;

  # broken
  #checkInputs = [
  #  flake8
  #  flake8-import-order
  #  hacking
  #  mock
  #  tox
  #  yapf
  #];

  #checkPhase = "";

  pythonImportsCheck = [ "vcd_cli" ];

  meta = with lib; {
    description = "Command Line Interface for VMware vCloud Director";
    homepage = "https://github.com/vmware/vcd-cli";
    license = licenses.asl20;
  };
}
